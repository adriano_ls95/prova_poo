package prova_poo;

public class Principal {

	public static void main(String[] args) {
		Livro livro = new Livro();
		
		livro.setAutor("Jose Manoel");
		livro.setEdicao("1 edição");
		livro.setVolume("Volume 1");
		
		Item item = new Item();
		
		item.ano_Publicacao = "2012";
		item.isbn = "9493920";
		item.preco = "RS 60,00";
		item.editora= "Veja";
		item.titulo = "O Pai";
	
		System.out.printf("Autor:%s, Ed:%s e Vol:%s",livro.getAutor(),livro.getEdicao(),livro.getVolume());
	}

}
