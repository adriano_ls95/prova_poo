package prova_poo;

public class Item {
	
	String titulo;
	String editora;
	String ano_Publicacao;
	String isbn;
	String preco;
	
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public String getAno_Publicacao() {
		return ano_Publicacao;
	}
	public void setAno_Publicacao(String ano_Publicacao) {
		this.ano_Publicacao = ano_Publicacao;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	public void display() {
		// TODO Auto-generated method stub
		
	}
	

}
